package org.eclipse.openk.contactbasedata.config;

import jakarta.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.service.LdapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Log4j2
@Configuration
@EnableScheduling
@ConditionalOnProperty(prefix = "ldap-sync.scheduling", name="enabled", havingValue="true", matchIfMissing = false)
public class LdapSchedulerConfig {

    @Autowired
    LdapService ldapService;

    @Value("${ldap-sync.scheduling.cron-expression}")
    private String cronExpression;

    @PostConstruct
    public void logConfigLdap(){
        log.info("Scheduler is enabled with cron expression: " + cronExpression);
    }

    @Scheduled(cron = "${ldap-sync.scheduling.cron-expression}")
    public void scheduleTaskSynchronize() {
        log.info("Executing scheduled task: Synchronizing Users with Ldap");
        ldapService.synchronizeLDAP();
        log.info("Finished scheduled task: Synchronizing Users with Ldap");
    }
}
