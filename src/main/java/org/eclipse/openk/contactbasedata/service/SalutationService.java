/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.service;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.constants.Constants;
import org.eclipse.openk.contactbasedata.exceptions.BadRequestException;
import org.eclipse.openk.contactbasedata.exceptions.NotFoundException;
import org.eclipse.openk.contactbasedata.mapper.SalutationMapper;
import org.eclipse.openk.contactbasedata.model.RefSalutation;
import org.eclipse.openk.contactbasedata.repository.SalutationRepository;
import org.eclipse.openk.contactbasedata.viewmodel.SalutationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Log4j2
@Service
public class SalutationService{
    @Autowired
    private SalutationRepository salutationRepository;

    @Autowired
    SalutationMapper salutationMapper;

    public List<SalutationDto> findAllSalutations() {
        return salutationRepository.findAll().stream()
                    .map( salutationMapper::toSalutationDto )
                    .collect(Collectors.toList());
    }


    public SalutationDto getSalutationByUuid(UUID salutationUuid) {
        RefSalutation refSalutation = salutationRepository
                .findByUuid(salutationUuid)
                .orElseThrow(() -> new NotFoundException(Constants.SALUTATION_UUID_NOT_EXISTING));
        return salutationMapper.toSalutationDto(refSalutation);
    }

    @Transactional
    public SalutationDto insertSalutation(SalutationDto salutationDto) {
        RefSalutation salutationToSave = salutationMapper.toRefSalutation(salutationDto);
        salutationToSave.setUuid(UUID.randomUUID());

        RefSalutation savedSalutation = salutationRepository.save(salutationToSave);
        return salutationMapper.toSalutationDto(savedSalutation);
    }

    @Transactional
    public SalutationDto updateSalutation(SalutationDto salutationDto){
        RefSalutation salutationUpdated;
        RefSalutation salutationToSave = salutationMapper.toRefSalutation(salutationDto);
        RefSalutation existingSalutation = salutationRepository
                .findByUuid(salutationDto.getUuid())
                .orElseThrow(() -> new NotFoundException(Constants.SALUTATION_UUID_NOT_EXISTING));
        salutationToSave.setId(existingSalutation.getId());
        salutationUpdated = salutationRepository.save(salutationToSave);

        return salutationMapper.toSalutationDto(salutationUpdated);
    }

    @Transactional
    public void removeSalutation(UUID uuid) {
        RefSalutation existingSalutation = salutationRepository.findByUuid(uuid)
                .orElseThrow( () -> new BadRequestException(Constants.SALUTATION_UUID_NOT_EXISTING));

        salutationRepository.delete(existingSalutation);
    }

}
