/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.repository;

import org.eclipse.openk.contactbasedata.model.TblAssignmentModulContact;
import org.eclipse.openk.contactbasedata.model.TblContact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface AssignmentModulContactRepository extends JpaRepository<TblAssignmentModulContact, Long> {

    public Optional<TblAssignmentModulContact> findByUuid(UUID uuid);

    Optional<TblAssignmentModulContact> findByTblContactAndUuid(TblContact tblContact, UUID uuid);

    @Query("select a from TblAssignmentModulContact a where a.tblContact.uuid = ?1")
    public List< TblAssignmentModulContact > findByTblContactUuid(final UUID contactUuid);

    @Query("select count(*) from TblAssignmentModulContact a where a.tblContact.id=:contactId and a.modulName=:modulName")
    Long countByContactIdAndModuleName(@Param("contactId") Long contactId, @Param("modulName") String modulName);

    @Query("select count(*) from TblAssignmentModulContact a where a.tblContact.id=:contactId and a.modulName= :modulName and a.uuid <> :assignmentUuid")
    Long countByContactIdAndUuidAndModulNameIsNotSame(@Param("contactId") Long supplierId, @Param("assignmentUuid") UUID assignmentUuid , @Param("modulName") String modulName);

}


