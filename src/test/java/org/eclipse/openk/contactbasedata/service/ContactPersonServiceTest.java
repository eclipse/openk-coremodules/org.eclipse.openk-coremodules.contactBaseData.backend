/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.service;

import org.eclipse.openk.contactbasedata.config.TestConfiguration;
import org.eclipse.openk.contactbasedata.exceptions.NotFoundException;
import org.eclipse.openk.contactbasedata.model.RefPersonType;
import org.eclipse.openk.contactbasedata.model.RefSalutation;
import org.eclipse.openk.contactbasedata.model.TblCompany;
import org.eclipse.openk.contactbasedata.model.TblContact;
import org.eclipse.openk.contactbasedata.model.TblContactPerson;
import org.eclipse.openk.contactbasedata.repository.CompanyRepository;
import org.eclipse.openk.contactbasedata.repository.ContactPersonRepository;
import org.eclipse.openk.contactbasedata.repository.ContactRepository;
import org.eclipse.openk.contactbasedata.repository.PersonTypeRepository;
import org.eclipse.openk.contactbasedata.repository.SalutationRepository;
import org.eclipse.openk.contactbasedata.support.MockDataHelper;
import org.eclipse.openk.contactbasedata.viewmodel.ContactPersonDto;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
class ContactPersonServiceTest {
    @Qualifier("myContactPersonService")
    @Autowired
    private ContactPersonService contactPersonService;

    @MockBean
    private ContactPersonRepository contactPersonRepository;

    @MockBean
    private ContactRepository contactRepository;

    @MockBean
    private SalutationRepository salutationRepository;

    @MockBean
    private PersonTypeRepository personTypeRepository;

    @MockBean
    private CompanyRepository companyRepository;

    @Test
    void shouldFindAContactPersonProperly() {
        TblContactPerson cp = MockDataHelper.mockTblContactPerson();
        when(contactPersonRepository.findByTblContactUuid(any(UUID.class))).thenReturn(Optional.of(cp));
        ContactPersonDto dto = contactPersonService.findContactPerson(UUID.randomUUID());

        assertEquals(cp.getContact().getUuid(), dto.getContactUuid());
    }

    @Test
    void shouldThrowNotFoundErrorContactPerson() {
        TblContactPerson cp = MockDataHelper.mockTblContactPerson();
        when(contactPersonRepository.findByTblContactUuid(any(UUID.class))).thenReturn(Optional.empty());
        assertThrows(NotFoundException.class, () -> contactPersonService.findContactPerson(UUID.randomUUID()));
    }

    @Test
    void shouldUpdateContactPerson() {
        ContactPersonDto cpDto = MockDataHelper.mockContactPersonDto();
        TblContactPerson cpTbl = MockDataHelper.mockTblContactPerson();

        when(contactPersonRepository.findByTblContactUuid(any(UUID.class))).thenReturn(Optional.of(cpTbl));
        when(salutationRepository.findByUuid(any(UUID.class)))
                .thenReturn(Optional.of(MockDataHelper.mockRefSalutation()));
        when(personTypeRepository.findByUuid(any(UUID.class)))
                .thenReturn(Optional.of(MockDataHelper.mockRefPersonType()));
        when(companyRepository.findByTblContactUuid(any(UUID.class)))
                .thenReturn(Optional.of(MockDataHelper.mockCompany()));
//        when(contactPersonRepository.save(any(TblContactPerson.class)))
//                .thenReturn(cpTbl);
        when(contactPersonRepository.save(any(TblContactPerson.class)))
                .then((Answer<TblContactPerson>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblContactPerson) args[0];
                });

        ContactPersonDto savedDto = contactPersonService.updateContactPerson(cpDto);

        assertEquals(cpTbl.getContact().getUuid(), savedDto.getContactUuid());
        assertEquals(cpTbl.getContact().getNote(), savedDto.getContactNote());
        assertEquals(cpTbl.getContact().getContactType(), savedDto.getContactType());
        assertEquals(cpTbl.getFirstName(), savedDto.getFirstName());
        assertEquals(cpTbl.getLastName(), savedDto.getLastName());
        assertEquals(cpTbl.getTitle(), savedDto.getTitle());
        assertEquals(cpTbl.getRefPersonType().getUuid(), savedDto.getPersonTypeUuid());
        assertEquals(cpTbl.getSalutation().getUuid(), savedDto.getSalutationUuid());
    }


    @Test
    void shouldNotUpdateContactPerson_Exception1() {
        ContactPersonDto epDto = MockDataHelper.mockContactPersonDto();
        TblContactPerson cpTbl = MockDataHelper.mockTblContactPerson();
        when(contactPersonRepository.findByTblContactUuid(any(UUID.class))).thenReturn(Optional.empty());
        when(salutationRepository.findByUuid(any(UUID.class)))
                .thenReturn(Optional.of(MockDataHelper.mockRefSalutation()));
        when(personTypeRepository.findByUuid(any(UUID.class)))
                .thenReturn(Optional.of(MockDataHelper.mockRefPersonType()));

        when(contactPersonRepository.save(any(TblContactPerson.class)))
                .then((Answer<TblContactPerson>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblContactPerson) args[0];
                });

        assertThrows( NotFoundException.class, () -> contactPersonService.updateContactPerson(epDto));
    }

    @Test
    void shouldNotUpdateContactPerson_Exception2() {
        ContactPersonDto cpDto = MockDataHelper.mockContactPersonDto();
        TblContactPerson cpTbl = MockDataHelper.mockTblContactPerson();
        when(contactPersonRepository.findByTblContactUuid(any(UUID.class))).thenReturn(Optional.of(cpTbl));
        when(salutationRepository.findByUuid(any(UUID.class)))
                .thenReturn(Optional.empty());
        when(personTypeRepository.findByUuid(any(UUID.class)))
                .thenReturn(Optional.of(MockDataHelper.mockRefPersonType()));

        when(contactPersonRepository.save(any(TblContactPerson.class)))
                .then((Answer<TblContactPerson>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblContactPerson) args[0];
                });

        assertThrows( NotFoundException.class, () -> contactPersonService.updateContactPerson(cpDto));
    }


    @Test
    void shouldInsertContactPerson() {
        ContactPersonDto cpDto = MockDataHelper.mockContactPersonDto();
        cpDto.setContactUuid(null);
        RefSalutation refSalutation = MockDataHelper.mockRefSalutation();
        when(salutationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refSalutation));
        RefPersonType refPersonType = MockDataHelper.mockRefPersonType();
        when(personTypeRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refPersonType));
        TblCompany tblCompany = MockDataHelper.mockCompany();
        when(companyRepository.findByTblContactUuid(any(UUID.class))).thenReturn(Optional.of(tblCompany));

        when(contactRepository.save(any(TblContact.class)))
                .then((Answer<TblContact>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblContact) args[0];
                });
        when(contactPersonRepository.save(any(TblContactPerson.class)))
                .then((Answer<TblContactPerson>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblContactPerson) args[0];
                });

        ContactPersonDto savedDto = contactPersonService.insertContactPerson(cpDto);
        assertNotNull(savedDto.getContactUuid());
        assertEquals(cpDto.getContactNote(), savedDto.getContactNote());
        assertEquals(cpDto.getContactType(), savedDto.getContactType());
        assertEquals(cpDto.getFirstName(), savedDto.getFirstName());
        assertEquals(cpDto.getLastName(), savedDto.getLastName());
        assertEquals(cpDto.getTitle(), savedDto.getTitle());
        assertEquals(refPersonType.getUuid(), savedDto.getPersonTypeUuid());
        assertEquals(refSalutation.getUuid(), savedDto.getSalutationUuid());
    }
    
    @Test
    void shouldDeleteContactPerson() {
        TblContactPerson tblContactPerson = MockDataHelper.mockTblContactPerson();
        TblContact tblContact = MockDataHelper.mockTblContact();
        tblContactPerson.setContact(tblContact);

        when (contactRepository.findByUuid( any(UUID.class))).thenReturn(Optional.of(tblContact));
        when(contactPersonRepository.findByTblContactUuid(any(UUID.class))).thenReturn(Optional.of(tblContactPerson));
        Mockito.doNothing().when(contactPersonRepository).delete( isA( TblContactPerson.class ));
        Mockito.doNothing().when(contactRepository).delete( isA( TblContact.class ));

        contactPersonService.deleteContactPerson(tblContact.getUuid());

        Mockito.verify(contactPersonRepository, times(1)).delete( tblContactPerson );
        Mockito.verify(contactRepository, times(1)).delete( tblContact );
    }
}