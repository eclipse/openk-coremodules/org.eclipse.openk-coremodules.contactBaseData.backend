FROM eclipse-temurin:17-jre-alpine
EXPOSE 9155
COPY /target/contact-base-data.jar /usr/src/cbd/
WORKDIR usr/src/cbd
CMD ["java", "-jar", "-Dspring.config.name=application_localdev", "contact-base-data.jar"] 
