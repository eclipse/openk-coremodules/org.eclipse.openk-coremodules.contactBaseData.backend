#!/bin/sh
echo ------- Login Keycloak -------
sh kcadm.sh config credentials --server http://localhost:8380/auth --realm master --user admin --password admin
realmVar="OpenKRealm"
# ***************** CREATING NEW USER *****************
usernameVar="finia_r"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Finia -s lastName=Reader  -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename kon-access --rolename kon-reader -r $realmVar
echo roles set
# ***************** CREATING NEW USER *****************
usernameVar="emilia_w"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Emilia -s lastName=Writer  -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename kon-access --rolename kon-writer -r $realmVar
echo roles set
# ***************** CREATING NEW USER *****************
usernameVar="lea_a"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Lea -s lastName=Admin  -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename kon-access --rolename kon-admin -r $realmVar
echo roles set
# ***************** CREATING NEW USER *****************
usernameVar="leon_r"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Leon -s lastName=Reader  -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename kon-access --rolename kon-reader -r $realmVar
echo roles set
# ***************** CREATING NEW USER *****************
usernameVar="david_w"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=David -s lastName=Writer  -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename kon-access --rolename kon-writer -r $realmVar
echo roles set
# ***************** CREATING NEW USER *****************
usernameVar="anton_a"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Anton -s lastName=Admin  -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename kon-access --rolename kon-admin -r $realmVar
echo roles set

echo ------- Finished -------
